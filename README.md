SSH CLIENT IMAGE
----------------

This lightweight image contains only ssh client for ssh operations on remote hosts

1. Added `openssh-client`
2. Added `ca-certificates`
3. Added `bash`
