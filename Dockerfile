FROM    alpine:3.10

ARG     OPENSSH_CLIENT_VERSION
ARG     CA_CERTIFICATES_VERSION
ARG     BASH_VERSION

RUN     apk update \
          && apk add \
              openssh-client=${OPENSSH_CLIENT_VERSION} \
              ca-certificates=${CA_CERTIFICATES_VERSION} \
              bash=${BASH_VERSION}
